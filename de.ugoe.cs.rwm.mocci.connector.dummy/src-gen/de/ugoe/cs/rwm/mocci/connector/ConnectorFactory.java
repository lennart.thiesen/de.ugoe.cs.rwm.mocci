/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Thu Jan 03 13:17:39 CET 2019 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

/**
 * Connector EFactory for the OCCI extension: - name: monitoring - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring#
 */
public class ConnectorFactory extends monitoring.impl.MonitoringFactoryImpl {
	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: sensor - title: Sensor
	 * Component
	 */
	@Override
	public monitoring.Sensor createSensor() {
		return new SensorConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: datagatherer - title:
	 * DataGatherer Resource
	 */
	@Override
	public monitoring.Datagatherer createDatagatherer() {
		return new DatagathererConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: processor - title: Processor
	 * Resource
	 */
	@Override
	public monitoring.Dataprocessor createDataprocessor() {
		return new DataprocessorConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: publisher - title: Publisher
	 * Resource
	 */
	@Override
	public monitoring.Resultprovider createResultprovider() {
		return new ResultproviderConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: martpublisher - title:
	 * MartPublisher Mixin
	 */
	@Override
	public monitoring.Occiresultprovider createOcciresultprovider() {
		return new OcciresultproviderConnector();
	}

	/**
	 * EFactory method for OCCI kind: - scheme:
	 * http://schemas.ugoe.cs.rwm/monitoring# - term: monitoringproperty - title:
	 * MonitoringProperty Component
	 */
	@Override
	public monitoring.Monitorableproperty createMonitorableproperty() {
		return new MonitorablepropertyConnector();
	}
}
