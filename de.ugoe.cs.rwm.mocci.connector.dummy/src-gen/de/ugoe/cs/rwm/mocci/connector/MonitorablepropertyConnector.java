/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Thu Jan 03 13:17:39 CET 2019 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: monitoringproperty - title:
 * MonitoringProperty Component
 */
public class MonitorablepropertyConnector extends monitoring.impl.MonitorablepropertyImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(MonitorablepropertyConnector.class);

	// Start of user code Monitoringpropertyconnector_constructor
	/**
	 * Constructs a monitoringproperty connector.
	 */
	MonitorablepropertyConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code MonitoringpropertyocciCreate
	/**
	 * Called when this Monitoringproperty instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Monitoringproperty_occiRetrieve_method
	/**
	 * Called when this Monitoringproperty instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Monitoringproperty_occiUpdate_method
	/**
	 * Called when this Monitoringproperty instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code MonitoringpropertyocciDelete_method
	/**
	 * Called when this Monitoringproperty instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Monitoringproperty actions.
	//

}
