/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Wed Dec 19 10:30:40 CET 2018 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import org.modmacao.occi.platform.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: datagatherer - title:
 * DataGatherer Resource
 */
public class DatagathererConnector extends monitoring.impl.DatagathererImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(DatagathererConnector.class);

	// Start of user code Datagathererconnector_constructor
	/**
	 * Constructs a datagatherer connector.
	 */
	DatagathererConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code DatagathererocciCreate
	/**
	 * Called when this Datagatherer instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Datagatherer_occiRetrieve_method
	/**
	 * Called when this Datagatherer instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Datagatherer_occiUpdate_method
	/**
	 * Called when this Datagatherer instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code DatagathererocciDelete_method
	/**
	 * Called when this Datagatherer instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Datagatherer actions.
	//

	// Start of user code Datagatherer_Kind_configure_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: configure
	 * - title:
	 */
	@Override
	public void configure() {
		LOGGER.debug("Action configure() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.DEPLOYED_VALUE:
			this.setOcciComponentState(Status.INACTIVE);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Datagatherer_Kind_deploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: deploy -
	 * title:
	 */
	@Override
	public void deploy() {
		LOGGER.debug("Action deploy() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.UNDEPLOYED_VALUE:
			this.setOcciComponentState(Status.DEPLOYED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Datagatherer_Kind_undeploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: undeploy
	 * - title:
	 */
	@Override
	public void undeploy() {
		LOGGER.debug("Action undeploy() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.ACTIVE_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.INACTIVE_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.DEPLOYED_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.ERROR_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Datagatherer_Kind_Stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: stop -
	 * title: Stop the application.
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);
		setOcciComponentState(Status.INACTIVE);
	}

	// End of user code
	// Start of user code Datagatherer_Kind_Start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: start -
	 * title: Start the application.
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.INACTIVE_VALUE:
			this.setOcciComponentState(Status.ACTIVE);
			break;

		case Status.UNDEPLOYED_VALUE:
			this.setOcciComponentState(Status.ACTIVE);
			break;

		default:
			break;

		}
	}
	// End of user code

}
