/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Wed Dec 19 10:30:40 CET 2018 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.Link;
import org.modmacao.occi.platform.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import monitoring.Monitorableproperty;
import monitoring.Sensor;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: publisher - title: Publisher
 * Resource
 */
public class ResultproviderConnector extends monitoring.impl.ResultproviderImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(ResultproviderConnector.class);
	private Thread simulation;
	// private Occiresultprovider occiResProv;

	// Start of user code Publisherconnector_constructor
	/**
	 * Constructs a publisher connector.
	 */
	ResultproviderConnector() {
		LOGGER.debug("Constructor called on " + this);

	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code PublisherocciCreate
	/**
	 * Called when this Publisher instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);

		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Publisher_occiRetrieve_method
	/**
	 * Called when this Publisher instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);

		if (this.getOcciComponentState().getValue() == Status.ACTIVE.getValue()) {
			if (simulation == null) {
				ResultproviderSimulation sim = new ResultproviderSimulation(getMonProp(getSensor()));
				simulation = new Thread(sim);
				simulation.start();
			}
		}
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Publisher_occiUpdate_method
	/**
	 * Called when this Publisher instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code PublisherocciDelete_method
	/**
	 * Called when this Publisher instance will be deleted.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		if (simulation != null) {
			simulation.stop();
		}
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	//
	// Publisher actions.
	//

	// Start of user code Publisher_Kind_configure_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: configure
	 * - title:
	 */
	@Override
	public void configure() {
		LOGGER.debug("Action configure() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.DEPLOYED_VALUE:
			this.setOcciComponentState(Status.INACTIVE);
			break;

		default:
			break;
		}
		// TODO: Implement how to configure this publisher.
	}

	// End of user code
	// Start of user code Publisher_Kind_deploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: deploy -
	 * title:
	 */
	@Override
	public void deploy() {
		LOGGER.debug("Action deploy() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.UNDEPLOYED_VALUE:
			this.setOcciComponentState(Status.DEPLOYED);
			break;

		default:
			break;
		}

	}

	// End of user code
	// Start of user code Publisher_Kind_undeploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: undeploy
	 * - title:
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void undeploy() {
		LOGGER.debug("Action undeploy() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.ACTIVE_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			if (simulation != null) {
				simulation.stop();
			}
			break;

		case Status.INACTIVE_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.DEPLOYED_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		case Status.ERROR_VALUE:
			this.setOcciComponentState(Status.UNDEPLOYED);
			break;

		default:
			break;
		}
	}

	// End of user code
	// Start of user code Publisher_Kind_Stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: stop -
	 * title: Stop the application.
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);
		if (simulation != null) {
			simulation.stop();
		}
		setOcciComponentState(Status.INACTIVE);
	}

	// End of user code
	// Start of user code Publisher_Kind_Start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: start -
	 * title: Start the application.
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		switch (this.getOcciComponentState().getValue()) {

		case Status.INACTIVE_VALUE:
			this.setOcciComponentState(Status.ACTIVE);
			if (simulation == null || simulation.isAlive() == false) {
				ResultproviderSimulation sim = new ResultproviderSimulation(getMonProp(getSensor()));
				simulation = new Thread(sim);
				simulation.start();
			}
			break;

		case Status.UNDEPLOYED_VALUE:
			this.setOcciComponentState(Status.ACTIVE);
			if (simulation == null) {
				ResultproviderSimulation sim = new ResultproviderSimulation(getMonProp(getSensor()));
				simulation = new Thread(sim);
				simulation.start();
			}
			break;

		default:
			break;

		}
	}

	// End of user code

	private Sensor getSensor() {
		for (Link link : this.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return ((Sensor) link.getSource());
			}
		}
		throw new NoSuchElementException("No containing sensor found!");
	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

}
