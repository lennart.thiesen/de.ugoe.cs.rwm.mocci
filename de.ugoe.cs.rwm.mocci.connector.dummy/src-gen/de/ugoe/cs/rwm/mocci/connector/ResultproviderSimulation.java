/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci.connector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.impl.OCCIFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import monitoring.Monitorableproperty;

public class ResultproviderSimulation implements Runnable {
	private static Logger LOGGER = LoggerFactory.getLogger(ResultproviderSimulation.class);
	private Monitorableproperty monProp;
	private OCCIFactoryImpl factory = new OCCIFactoryImpl();
	private int interval;
	private List<String> results;

	public ResultproviderSimulation(Monitorableproperty monProp) {
		this.monProp = monProp;
		String property = getProperty(monProp.getMonitoringProperty());
		List<String> items = new ArrayList<String>(Arrays.asList(property.split("\\s*,\\s*")));
		this.interval = Integer.parseInt(items.get(items.size() - 1));
		items.remove(items.get(items.size() - 1));
		this.results = items;
		LOGGER.info("Creating Simulation for: " + monProp.getMonitoringProperty() + "; with values: " + results
				+ "; and interval: " + interval);
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			AttributeState monPropAttr = factory.createAttributeState();
			monPropAttr.setName("monitoring.result");
			int randomElementIndex = ThreadLocalRandom.current().nextInt(results.size());
			String value = results.get(randomElementIndex);
			monPropAttr.setValue(value);
			monProp.setMonitoringResult(value);
			LOGGER.info(
					"MonProp: " + monProp.getMonitoringProperty() + ", set to: " + value + "(" + monProp.getId() + ")");
			monProp.getAttributes().add(monPropAttr);
		}

	}

	private String getProperty(String monitoringProperty) {
		return new ResultproviderHelper().getProperties().getProperty(monitoringProperty);

	}

}
