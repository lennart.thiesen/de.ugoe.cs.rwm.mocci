package de.ugoe.cs.rwm.mocci.connector.dummy;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.Test;

import de.ugoe.cs.rwm.mocci.connector.ResultproviderHelper;

public class HelperTest {

	@Test
	public void testGetProperties() {
		ResultproviderHelper helper = new ResultproviderHelper();
		Properties props = helper.getProperties();
		assertEquals("None,Low,Medium,High,Critical,3000", props.getProperty("CPU"));
	}
}
