package de.ugoe.cs.rwm.mocci.connector.dummy;

import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.modmacao.occi.platform.PlatformPackage;

import monitoring.MonitoringPackage;

public class TestUtility {
	public static void extensionRegistrySetup() {
		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				PlatformPackage.class.getClassLoader().getResource("model/platform.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				MonitoringPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());

	}
}
