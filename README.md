# MOCCI
[![build status](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/badges/master/pipeline.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/commits/master)

MOCCI is an extension for the [Open Cloud Computing Interface (OCCI)](http://occi-wg.org/about/specification/) to enable a model-driven management of monitoring devices in the cloud, as well as storing their results within an OCCI-compliant runtime model. Together with other tools from the OCCI ecosystem it provides a testing and execution environment for self-adaptive systems with an EMF-based architecture runtime model ([Snapshot in JSON format](./doc/browser.png)). In the following you will find a getting started guide in which a preconfigured VirtualBox image is downloaded to perform example scenarios and an tutorial on how to enrich existing OCCI models with monitoring functionality. Moreover, an introduction to MOCCI's components is provided, as well as links and description on how to integrate MOCCI with other pre-existing tooling from the OCCI ecosystem. The paper submitted to this artifact and the VirtualBox image can be found [here](https://owncloud.gwdg.de/index.php/s/5u2ddnyyNlzecM5) with the password being mocci.


## Getting Started
To get started with MOCCI we provide a hands on experience in form of a VirtualBox image in which everything required is already configured and ready to go.
Alternatively, to manually setup the single components instructions can be found at the end of this document.

### Download VirtualBox Image
To get a quick start into MOCCI download the VM image (Mocci.ova) located here:
```
https://owncloud.gwdg.de/index.php/s/5u2ddnyyNlzecM5
password: mocci
```
This image contains a version of OCCI-Studio with the MoDMaCAO and MOCCI plugins pre-installed.
Moreover, it contains a checked out version of MOCCI and a local version of the MARTserver using dummy connectors.

After the download has finished, open VirtualBox and import the virtual appliance:
1. Click on File->Import Appliance...
2. Choose the destination of the downloaded image.
3. Adjust the appliance settings according to your System.
   1. For a smooth experience 8Gb Ram, and 2 CPUs are recommended.
4. Press Import.

Next start the Virtual Machine:
1. Choose the recently imported VM and press Start.
2. The username, and password are both: mocci

### Execute Step-by-step Example Scenarios and Tutorials
To learn the ropes of MOCCI, we provide step-by-step instructions for three example scenarios, which are based on the same initial deployment model.
It is recommended to execute the tutorials and examples in the following order:

1. [Initial Deployment](doc/initial.md): This tutorial demonstrates how to deploy an initial cloud application getting monitored by MOCCI.
2. [Vertical Scaling](doc/vertical.md): This scenario scales a VM in the initial deployment up and down according to its CPU utilization.
3. [Horizontal Scaling](doc/horizontal.md): This scenario dynamically adds and releases worker nodes in the hadoop cluster of the initial deployment.
4. [Sensor Creation](doc/own.md): This tutorial shows how to add sensors to an OCCI model.

## MOCCI Components, Documentation, and Related Artifacts
MOCCI itself is a monitoring extension that in combination with pre-existing software from the OCCI ecosystem allows for an easy development and testing of self-adaptation engines for cloud systems.
This section provides an overview of MOCCI's components, as well as other tools used to develop and perform the example scenarios.

### MOCCI (artifact)
MOCCI consists of three main components. The metamodel extension for OCCI, a connector responsible to deploy, configure, and manage arbitrary sensors and monitoring devices in the cloud using configuration management scripts, and a dummy connector simulating user defined monitoring results for test purposes. Descriptions of the single components can be found via the following links:
In addition to the getting started and the example scenarios discussed in this document, the main components of MOCCI are represented by the:
1. [Monitoring Extension](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.model)
2. [Monitoring Connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector)
3. [Monitoring Dummy Connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector.dummy)

### OCCI-Studio and MoDMaCAO (pre-existing Software from the OCCI ecosystem)
[OCCI-Studio](https://github.com/occiware/OCCI-Studio) is an IDE providing many convenient tools to develop around OCCI. For example, it provides a model editor, graphically and textually. Moreover, it allows to design OCCI extensions and automatically generate code from it.
In addition to OCCI-Studio the [Model-Driven Configuration Management of Cloud Applications with OCCI (MoDMaCAO)](https://github.com/occiware/MoDMaCAO) extension suite is needed.

[Documentation on how to setup and configure OCCI-Studio with MOCCI](doc/studio.md)


### MartServer (pre-existing Software from the OCCI ecosystem)
The [MartServer](https://github.com/occiware/MartServer) represents the OCCI interface to which requests are send in order to build and monitor the cloud application.
This server is easy to extend by plugging in connectors created for modeled OCCI extensions.

[Documentation on how to setup and configure the MartServer](doc/openstack.md)


*Note:* Please note that the execution of the example scenarios in an distributed environment requires access and  connectors to the cloud. 
Thus, the provided scenarios in the getting started VM is only executed based on monitoring data simulated by the [MOCCI connector dummy](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector.dummy).

