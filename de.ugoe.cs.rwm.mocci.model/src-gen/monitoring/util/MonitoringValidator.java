/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package monitoring.util;

import java.util.Map;

import org.eclipse.cmf.occi.core.util.OCCIValidator;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EObjectValidator;

import monitoring.Datagatherer;
import monitoring.Dataprocessor;
import monitoring.Monitorableproperty;
import monitoring.MonitoringPackage;
import monitoring.Occiresultprovider;
import monitoring.Resultprovider;
import monitoring.Sensor;

/**
 * <!-- begin-user-doc --> The <b>Validator</b> for the model. <!-- end-user-doc
 * -->
 * 
 * @see monitoring.MonitoringPackage
 * @generated
 */
public class MonitoringValidator extends EObjectValidator {
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final MonitoringValidator INSTANCE = new MonitoringValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource()
	 * source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode()
	 * codes} from this package. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "monitoring";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for
	 * constraint 'Target Constraint' of 'Monitorableproperty'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int MONITORABLEPROPERTY__TARGET_CONSTRAINT = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for
	 * constraint 'Applies Constraint' of 'Occiresultprovider'. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int OCCIRESULTPROVIDER__APPLIES_CONSTRAINT = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for
	 * additional hand written constants. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for
	 * additional hand written constants in a derived class. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	protected OCCIValidator occiValidator;

	/**
	 * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	public MonitoringValidator() {
		super();
		occiValidator = OCCIValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return MonitoringPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case MonitoringPackage.SENSOR:
			return validateSensor((Sensor) value, diagnostics, context);
		case MonitoringPackage.DATAGATHERER:
			return validateDatagatherer((Datagatherer) value, diagnostics, context);
		case MonitoringPackage.DATAPROCESSOR:
			return validateDataprocessor((Dataprocessor) value, diagnostics, context);
		case MonitoringPackage.RESULTPROVIDER:
			return validateResultprovider((Resultprovider) value, diagnostics, context);
		case MonitoringPackage.MONITORABLEPROPERTY:
			return validateMonitorableproperty((Monitorableproperty) value, diagnostics, context);
		case MonitoringPackage.OCCIRESULTPROVIDER:
			return validateOcciresultprovider((Occiresultprovider) value, diagnostics, context);
		case MonitoringPackage.STRING:
			return validateString((String) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateSensor(Sensor sensor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(sensor, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(sensor, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_IdUnique(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_AttributesNameUnique(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(sensor, diagnostics,
					context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_DifferentMixins(sensor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateResource_ResourceKindIsInParent(sensor, diagnostics, context);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateDatagatherer(Datagatherer datagatherer, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(datagatherer, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(datagatherer, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_IdUnique(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_AttributesNameUnique(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(datagatherer, diagnostics,
					context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_DifferentMixins(datagatherer, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateResource_ResourceKindIsInParent(datagatherer, diagnostics, context);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateDataprocessor(Dataprocessor dataprocessor, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(dataprocessor, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(dataprocessor, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_IdUnique(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_AttributesNameUnique(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(dataprocessor, diagnostics,
					context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_DifferentMixins(dataprocessor, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateResource_ResourceKindIsInParent(dataprocessor, diagnostics, context);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateResultprovider(Resultprovider resultprovider, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(resultprovider, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(resultprovider, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_IdUnique(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_AttributesNameUnique(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(resultprovider, diagnostics,
					context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_DifferentMixins(resultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateResource_ResourceKindIsInParent(resultprovider, diagnostics, context);
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateMonitorableproperty(Monitorableproperty monitorableproperty, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(monitorableproperty, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(monitorableproperty, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_IdUnique(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_AttributesNameUnique(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_KindCompatibleWithOneAppliesOfEachMixin(monitorableproperty,
					diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateEntity_DifferentMixins(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateLink_LinkKindIsInParent(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateLink_sourceReferenceInvariant(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= occiValidator.validateLink_targetReferenceInvariant(monitorableproperty, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validateMonitorableproperty_targetConstraint(monitorableproperty, diagnostics, context);
		}
		return result;
	}

	/**
	 * Validates the targetConstraint constraint of '<em>Monitorableproperty</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateMonitorableproperty_targetConstraint(Monitorableproperty monitorableproperty,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return monitorableproperty.targetConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateOcciresultprovider(Occiresultprovider occiresultprovider, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(occiresultprovider, diagnostics, context)) {
			return false;
		}
		boolean result = validate_EveryMultiplicityConforms(occiresultprovider, diagnostics, context);
		if (result || diagnostics != null) {
			result &= validate_EveryDataValueConforms(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryReferenceIsContained(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryBidirectionalReferenceIsPaired(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryProxyResolves(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_UniqueID(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryKeyUnique(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validate_EveryMapEntryUnique(occiresultprovider, diagnostics, context);
		}
		if (result || diagnostics != null) {
			result &= validateOcciresultprovider_appliesConstraint(occiresultprovider, diagnostics, context);
		}
		return result;
	}

	/**
	 * Validates the appliesConstraint constraint of '<em>Occiresultprovider</em>'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateOcciresultprovider_appliesConstraint(Occiresultprovider occiresultprovider,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return occiresultprovider.appliesConstraint(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean validateString(String string, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this
	 * validator's diagnostics. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this
		// validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} // MonitoringValidator
