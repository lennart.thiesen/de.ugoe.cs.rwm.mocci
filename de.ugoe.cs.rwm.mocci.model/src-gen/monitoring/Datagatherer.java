/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package monitoring;

import org.modmacao.occi.platform.Component;
 
/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Datagatherer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * DataGatherer Resource
 * <!-- end-model-doc -->
 *
 *
 * @see monitoring.MonitoringPackage#getDatagatherer()
 * @model
 * @generated
 */
public interface Datagatherer extends Component {

} // Datagatherer
