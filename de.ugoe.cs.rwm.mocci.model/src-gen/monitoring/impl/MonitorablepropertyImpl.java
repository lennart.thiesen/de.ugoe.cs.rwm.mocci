/**
 * Copyright (c) 2015-2017 Obeo, Inria
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 	
 * Contributors:
 * - William Piers <william.piers@obeo.fr>
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 */
package monitoring.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Map;

import monitoring.Monitorableproperty;
import monitoring.MonitoringPackage;
import monitoring.MonitoringTables;

import org.eclipse.cmf.occi.core.Resource;

import org.eclipse.cmf.occi.core.impl.LinkImpl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.pivot.evaluation.Executor;

import org.eclipse.ocl.pivot.ids.IdResolver;
import org.eclipse.ocl.pivot.ids.TypeId;

import org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal;

import org.eclipse.ocl.pivot.library.oclany.OclAnyOclIsKindOfOperation;
import org.eclipse.ocl.pivot.library.oclany.OclComparableLessThanEqualOperation;

import org.eclipse.ocl.pivot.library.string.CGStringGetSeverityOperation;
import org.eclipse.ocl.pivot.library.string.CGStringLogDiagnosticOperation;

import org.eclipse.ocl.pivot.utilities.ValueUtil;

import org.eclipse.ocl.pivot.values.IntegerValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitorableproperty</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link monitoring.impl.MonitorablepropertyImpl#getMonitoringProperty <em>Monitoring Property</em>}</li>
 *   <li>{@link monitoring.impl.MonitorablepropertyImpl#getMonitoringResult <em>Monitoring Result</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitorablepropertyImpl extends LinkImpl implements Monitorableproperty {
	/**
	 * The default value of the '{@link #getMonitoringProperty() <em>Monitoring Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringProperty()
	 * @generated
	 * @ordered
	 */
	protected static final String MONITORING_PROPERTY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonitoringProperty() <em>Monitoring Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringProperty()
	 * @generated
	 * @ordered
	 */
	protected String monitoringProperty = MONITORING_PROPERTY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMonitoringResult() <em>Monitoring Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringResult()
	 * @generated
	 * @ordered
	 */
	protected static final String MONITORING_RESULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMonitoringResult() <em>Monitoring Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoringResult()
	 * @generated
	 * @ordered
	 */
	protected String monitoringResult = MONITORING_RESULT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitorablepropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MonitoringPackage.Literals.MONITORABLEPROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMonitoringProperty() {
		return monitoringProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoringProperty(String newMonitoringProperty) {
		String oldMonitoringProperty = monitoringProperty;
		monitoringProperty = newMonitoringProperty;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MonitoringPackage.MONITORABLEPROPERTY__MONITORING_PROPERTY, oldMonitoringProperty, monitoringProperty));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMonitoringResult() {
		return monitoringResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMonitoringResult(String newMonitoringResult) {
		String oldMonitoringResult = monitoringResult;
		monitoringResult = newMonitoringResult;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MonitoringPackage.MONITORABLEPROPERTY__MONITORING_RESULT, oldMonitoringResult, monitoringResult));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean targetConstraint(final DiagnosticChain diagnostics, final Map<Object, Object> context) {
		/**
		 *
		 * inv targetConstraint:
		 *   let
		 *     severity : Integer[1] = 'Monitorableproperty::targetConstraint'.getSeverity()
		 *   in
		 *     if severity <= 0
		 *     then true
		 *     else
		 *       let result : occi::Boolean[1] = self.target.oclIsKindOf(Sensor)
		 *       in
		 *         'Monitorableproperty::targetConstraint'.logDiagnostic(self, null, diagnostics, context, null, severity, result, 0)
		 *     endif
		 */
		final /*@NonInvalid*/ Executor executor = PivotUtilInternal.getExecutor(this);
		final /*@NonInvalid*/ IdResolver idResolver = executor.getIdResolver();
		final /*@NonInvalid*/ IntegerValue severity_0 = CGStringGetSeverityOperation.INSTANCE.evaluate(executor, MonitoringTables.STR_Monitorableproperty_c_c_targetConstraint);
		final /*@NonInvalid*/ boolean le = OclComparableLessThanEqualOperation.INSTANCE.evaluate(executor, severity_0, MonitoringTables.INT_0).booleanValue();
		/*@NonInvalid*/ boolean symbol_0;
		if (le) {
			symbol_0 = ValueUtil.TRUE_VALUE;
		}
		else {
			final /*@NonInvalid*/ org.eclipse.ocl.pivot.Class TYP_monitoring_c_c_Sensor = idResolver.getClass(MonitoringTables.CLSSid_Sensor, null);
			final /*@NonInvalid*/ Resource target = this.getTarget();
			final /*@NonInvalid*/ boolean result = OclAnyOclIsKindOfOperation.INSTANCE.evaluate(executor, target, TYP_monitoring_c_c_Sensor).booleanValue();
			final /*@NonInvalid*/ boolean logDiagnostic = CGStringLogDiagnosticOperation.INSTANCE.evaluate(executor, TypeId.BOOLEAN, MonitoringTables.STR_Monitorableproperty_c_c_targetConstraint, this, (Object)null, diagnostics, context, (Object)null, severity_0, result, MonitoringTables.INT_0).booleanValue();
			symbol_0 = logDiagnostic;
		}
		return Boolean.TRUE == symbol_0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_PROPERTY:
				return getMonitoringProperty();
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_RESULT:
				return getMonitoringResult();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_PROPERTY:
				setMonitoringProperty((String)newValue);
				return;
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_RESULT:
				setMonitoringResult((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_PROPERTY:
				setMonitoringProperty(MONITORING_PROPERTY_EDEFAULT);
				return;
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_RESULT:
				setMonitoringResult(MONITORING_RESULT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_PROPERTY:
				return MONITORING_PROPERTY_EDEFAULT == null ? monitoringProperty != null : !MONITORING_PROPERTY_EDEFAULT.equals(monitoringProperty);
			case MonitoringPackage.MONITORABLEPROPERTY__MONITORING_RESULT:
				return MONITORING_RESULT_EDEFAULT == null ? monitoringResult != null : !MONITORING_RESULT_EDEFAULT.equals(monitoringResult);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MonitoringPackage.MONITORABLEPROPERTY___TARGET_CONSTRAINT__DIAGNOSTICCHAIN_MAP:
				return targetConstraint((DiagnosticChain)arguments.get(0), (Map<Object, Object>)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (monitoringProperty: ");
		result.append(monitoringProperty);
		result.append(", monitoringResult: ");
		result.append(monitoringResult);
		result.append(')');
		return result.toString();
	}

} //MonitorablepropertyImpl
