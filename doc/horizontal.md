# Horizontal Scaling Scenario
Prerequisite for this scenario is the [initial deployment](./initial.md) of the hadoop cluster.
In this scenario a hadoop cluster with one worker node is deployed and scaled according to gathered CPU utilization.
Therefore, a MAPE-k loop is initialized that periodically checks the CPU utilization of all worker nodes.
Thereafter, it is decided whether additional worker nodes are required (scaleUp) or not (scaleDown).
When a scaleUp is performed, a compute node hosting a hadoop-worker component is added to the model,
which gets executed over [DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) a models at runtime engine.


## Starting the MAPE-K loop
Before the MAPE-K loop is started make sure that the MartServer is running and the hadoop cluster has been deployed.
In this scenario, a Java application is started that utilizes the schematized data format of the OCCI metamodel and its extensions.
This scenario serves as an example on how to work with the OCCI ecosystem, including the monitoring extension and the models at runtime engine.
To start the MAPE-K loop execute MAPE.java as a Java Application.
In the VM it is located on the desktop. Open a new terminal (Ctrl-Alt-T) and navigate to the desktop (cd Desktop). You can start the loop by executing the following command:
```
java -jar MAPE.jar
```

### Mape-k loop - Output
The output of the script is separated into the individual steps of the MAPE loop: Monitor, Analyze, Plan, and Execute.
Thus, the output looks depending on the adaptive action to be performed similar to:

```
Starting MAPE loop

--------------------Waiting for new MAPE-K Cycle: Sleeping 10000--------------------
Monitor: Monitored CPUs: 1| Critical CPUs: 1| None CPUs: 0
Analyze: Critical State Detected
Plan: upScale!
         Adding Compute Node to Model
             Ip in Hadoop Network: 10.254.1.12
         Adding Worker Component to Model
         Adding Sensor to Model
             Adding Datagatherer to Model
             Adding Dataprocessor to Model
             Adding Resultprovider to Model
Execute: Deploying adjusted Model

```

In this case we queried for VMs with Critical and None CPU utilization. We detected only one being Critical. Thus, a critical state is detected for which an upscaling of the cluster is planned. Hereby, a compute node is added to the hadoop cluster with the ip 10.254.1.12, as well as the worker component hosted on this VM. Moreover, a Sensor including its monitoring devices are added to the model. These are responsible to monitor the newly added worker in the cluster. Before the changes get executed by putting the adjusted model into DOCCI, a model transformation is performed on it to add provider specific information to the model, e.g., an attachment of the new VM to the management network.

If more than one worker node is currently active in the cluster and a minimum of one has a CPU utilization of None the downscaling removes the VM with None utilization from the cluster. To investigate the expected behavior of this self-adaptive control loop you can check an example log [here](./horizontalLog.md). This log also includes all REST requests performed against the OCCI interface.

*Note*: If you want to get the same information as in the full log, including requests performed against the OCCI interface, you can alternatively start the MAPE_Exec_Info.jar.

### MartServer - Output

During the execution of the MAPE-K loop sensors are added and released to the running cloud deployment. Thus, different amounts of monitorableproperties are filled with values by the dummy connector. An excerpt can be found beneath. Moreover, it logs output about how each single request adjusting the runtime model are handled.
```
INFO  MonProp: CPU, set to: Low(ba16f4ee-1601-4192-a259-eae4274aed72)
INFO  MonProp: CPU, set to: High(e0053a21-7349-4918-bb1d-ecfffb2d4efb)
INFO  MonProp: CPU, set to: None(ba16f4ee-1601-4192-a259-eae4274aed72)

```

*Note*: The sometimes occurring MartServer error message regarding saving the model can be ignored as it has no effects on the cloud deployment nor is it related to the MOCCI extension artifact.

### Browser - Output
Again you can check the amount of deployed resources by using your browser. When a critical behavior is detected and an upscale is performed, the amount of compute nodes in the model is increased by one. This compute node hosts a hadoop worker component and has a sensor attached. When no critical behavior is detected, a downscale is planned, and execute the amount of provisioned compute nodes shrinks by one.

The amount of currently deployed sensors, applications, components, compute nodes, as well as their monitorableproperty can be accessed by the following example queries. Hereby, the information depicted can be updated by **refreshing the browser**.
```
http://localhost:8080/compute/
http://localhost:8080/sensor/
http://localhost:8080/monitorableproperty/
http://localhost:8080/application/
http://localhost:8080/component/
```

## Tuning the Scenario
Again the simulated monitoring results can be adjusted. Therefore, follow the steps defined the documentation of the [dummy connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector.dummy).
Moreover, the behaviour of the models at runtime engine can be investigated in more detail by adjusting specific Logger settings in the RegistryAndLoggerSetup.java file. Therefore,  open the OCCI-Studio and execute the scenario from here.
The class is located at:
```
de.ugoe.cs.rwm.mocci/src/main/java/MAPE.java
```

*Note:* To perform this scenario in a running cloud multiple adjustments have to be performed. In the code itself only the kind of connector has to be adjusted from LocalhostConnector to MartConnector. Moreover, an MartServer without actual connectors has to be created. We used the connectors located [here](../src/test/resources/martserver-plugins/live) and the ansible roles located [here](../src/test/resources/roles). Please refer to this [documentation](doc/openstack) to get started with actual cloud deployments.
