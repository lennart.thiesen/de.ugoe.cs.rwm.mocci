# Setting up OCCI-Studio

## Downloading OCCI-Studio
1. Download the [latest release of OCCI-Studio](https://github.com/occiware/OCCI-Studio/releases).
2. Extract OCCI-Studio and start it.

## Installing Required Plugins
To initialize the proposed OCCI extensions, the following plugins need to be added to the OCCI-Studio. 
These allow to correctly depict OCCI models in the textual and graphical editor. To Install plugins the following steps have
to be performed:

1. Navigate to: Help -> Install New Software...
2. Press Add...
3. Insert the location of the update site: 

### MoDMaCAO
The updatesite can be found at: [http://c109-125.cloud.gwdg.de/](http://c109-125.cloud.gwdg.de/).
To install the plugin please deselect "Group items by category" and choose MoDMaCAO.

### Gradle
To easily access our provided example install the gradle plugin within OCCI-Studio by following the instructions given here:
```
https://github.com/eclipse/buildship/blob/master/docs/user/Installation.md
```

### MOCCI
Finally, the MOCCI plugin can be installed which registers the OCCI monitoring extension. Therefore, the following steps have to be performed:
1. Navigate to [https://rwm.pages.gwdg.de/de.ugoe.cs.rwm.mocci/index.html](https://rwm.pages.gwdg.de/de.ugoe.cs.rwm.mocci/index.html):
2. Press Download Eclipse Plugin Archive
3. Save the updatesite.zip to any destination
4. Then follow the steps to install the plugin given above, but system choose the location of the updatesite.zip

Alternatively, you can build the Eclipse plugin yourself by checking out the project and perform the following command on the mocci.model project:
```
gradle updateSiteZip
```