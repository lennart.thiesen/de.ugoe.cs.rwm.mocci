# Setting up the MartServer
The MART Server implements the OCCI API used to orchestrate the Cloud deployments.
This is major component serving as entry point for our application.
In the following a description of how a Docker container for the MART Server can be created, stored, loaded, and started.

## Creating a Docker Container
In order to provide an easy use of the MART Server, a docker container embedding the server and all required plugins is provided.
However, to use specialized plugins currently the docker image has to be recreated. Thus, a brief explanation of the single steps are given in the following:

1. Install [docker](https://docs.docker.com/install/)
2. Clone the [MOCCI repository](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci)
3. Optional: Add martserver-plugins and roles to be used by the server. Adjust the authorized_keys file for ssh access.
4. Navigate to src/test/resources/
5. Create docker image: sudo docker build -t mart-server .
6. Test the docker image: sudo docker run -p 8080:8080 -p 22:22 mart-server
7. Store the docker image: sudo docker save mart-server $>$ mart-server.tar
8. To access the container you can use an ssh connection: ssh -i \$key root@localhost

To build this container a fatjar of the MartServer is used. To use newer versions please refer to the [documentation of the MartServer](https://github.com/occiware/MartServer/blob/master/doc/server.md) in how to create a docker container.

## Loading a Docker Container
To initialize the proposed OCCI extensions, the following plugins need to be added to the OCCI-Studio. 
These allow to correctly depict OCCI models in the textual and graphical editor. To Install plugins the following steps have
to be performed:

1. Download/Navigate to the archive containing the docker image
2. Load the docker image: docker load $<$ mart-server.tar
3. Start the image: sudo docker run -p 8080:8080 -p 22:22 mart-server
4. Start with bash: sudo docker run -p 8080:8080 -p 22:22 -i -t mart-server /bin/bash
5. To access the container you can use an ssh connection: ssh -i \$key root@localhost

## Configuring the MartServer to be used in OpenStack
[Documentation on how to setup and configure the MartServer for an OpenStack Cloud](doc/openstack.md)