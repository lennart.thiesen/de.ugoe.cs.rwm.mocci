/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Wed Dec 19 11:10:50 CET 2018 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: processor - title: Processor
 * Resource
 */
public class DataprocessorConnector extends monitoring.impl.DataprocessorImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(DataprocessorConnector.class);
	private ComponentManager compMan;

	// Start of user code Processorconnector_constructor
	/**
	 * Constructs a processor connector.
	 */
	DataprocessorConnector() {
		LOGGER.debug("Constructor called on " + this);
		this.compMan = new ComponentManager(this);
		// TODO: Implement this constructor.
	}
	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code ProcessorocciCreate
	/**
	 * Called when this Processor instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Processor_occiRetrieve_method
	/**
	 * Called when this Processor instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Processor_occiUpdate_method
	/**
	 * Called when this Processor instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code ProcessorocciDelete_method
	/**
	 * Called when this Processor instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		if (ComponentManager.isConnectedToCompute(this)) {
			this.stop();
			this.undeploy();
		}
	}
	// End of user code

	//
	// Processor actions.
	//

	// Start of user code Processor_Kind_deploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: deploy -
	 * title:
	 */
	@Override
	public void deploy() {
		LOGGER.debug("Action deploy() called on " + this);
		compMan.deploy();
		// TODO: Implement how to deploy this processor.
	}

	// End of user code
	// Start of user code Processor_Kind_Stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: stop -
	 * title: Stop the application.
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);
		compMan.stop();

		// TODO: Implement how to stop this processor.
	}

	// End of user code
	// Start of user code Processor_Kind_Start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: start -
	 * title: Start the application.
	 */
	@Override
	public void start() {
		LOGGER.debug("Action start() called on " + this);
		compMan.start();
		// TODO: Implement how to start this processor.
	}

	// End of user code
	// Start of user code Processor_Kind_configure_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: configure
	 * - title:
	 */
	@Override
	public void configure() {
		LOGGER.debug("Action configure() called on " + this);
		compMan.configure();
		// TODO: Implement how to configure this processor.
	}

	// End of user code
	// Start of user code Processor_Kind_undeploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: undeploy
	 * - title:
	 */
	@Override
	public void undeploy() {
		LOGGER.debug("Action undeploy() called on " + this);
		compMan.undeploy();
		// TODO: Implement how to undeploy this processor.
	}
	// End of user code

}
