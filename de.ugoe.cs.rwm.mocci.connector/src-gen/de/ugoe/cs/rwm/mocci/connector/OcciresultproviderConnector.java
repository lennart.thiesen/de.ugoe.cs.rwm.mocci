/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Wed Jan 02 16:14:48 CET 2019 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: martpublisher - title:
 * MartPublisher Mixin
 */
public class OcciresultproviderConnector extends monitoring.impl.OcciresultproviderImpl {
	/**
	 * Initialize the logger.
	 */
	private static Logger LOGGER = LoggerFactory.getLogger(OcciresultproviderConnector.class);

	// Start of user code Martpublisherconnector_constructor
	/**
	 * Constructs a martpublisher connector.
	 */
	OcciresultproviderConnector() {
		LOGGER.debug("Constructor called on " + this);
		// TODO: Implement this constructor.
	}
	// End of user code

}
