/**
 * Copyright (c) 2016-2017 Inria
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Philippe Merle <philippe.merle@inria.fr>
 * - Faiez Zalila <faiez.zalila@inria.fr>
 *
 * Generated at Wed Dec 19 11:10:50 CET 2018 from platform:/resource/monitoring/model/monitoring.occie by org.eclipse.cmf.occi.core.gen.connector
 */

package de.ugoe.cs.rwm.mocci.connector;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.core.impl.OCCIFactoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import monitoring.Monitorableproperty;
import monitoring.Occiresultprovider;
import monitoring.Sensor;

/**
 * Connector implementation for the OCCI kind: - scheme:
 * http://schemas.ugoe.cs.rwm/monitoring# - term: publisher - title: Publisher
 * Resource
 */
public class ResultproviderConnector extends monitoring.impl.ResultproviderImpl {
	/**
	 * Initialize the logger.
	 */
	private OCCIFactoryImpl factory = new OCCIFactoryImpl();
	private static Logger LOGGER = LoggerFactory.getLogger(ResultproviderConnector.class);
	private ComponentManager compMan;
	private Sensor sensor;
	private Monitorableproperty monProp;
	private Resource monObject;
	private List<AttributeState> workaround = new LinkedList<AttributeState>();

	// Start of user code Publisherconnector_constructor
	/**
	 * Constructs a publisher connector.
	 */
	ResultproviderConnector() {
		LOGGER.debug("Constructor called on " + this);
		this.compMan = new ComponentManager(this);

	}

	// End of user code
	//
	// OCCI CRUD callback operations.
	//

	// Start of user code PublisherocciCreate
	/**
	 * Called when this Publisher instance is completely created.
	 */
	@Override
	public void occiCreate() {
		LOGGER.debug("occiCreate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Publisher_occiRetrieve_method
	/**
	 * Called when this Publisher instance must be retrieved.
	 */
	@Override
	public void occiRetrieve() {
		LOGGER.debug("occiRetrieve() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code Publisher_occiUpdate_method
	/**
	 * Called when this Publisher instance is completely updated.
	 */
	@Override
	public void occiUpdate() {
		LOGGER.debug("occiUpdate() called on " + this);
		// TODO: Implement this callback or remove this method.
	}
	// End of user code

	// Start of user code PublisherocciDelete_method
	/**
	 * Called when this Publisher instance will be deleted.
	 */
	@Override
	public void occiDelete() {
		LOGGER.debug("occiDelete() called on " + this);
		if (ComponentManager.isConnectedToCompute(this)) {
			this.stop();
			this.undeploy();
		}
	}
	// End of user code

	//
	// Publisher actions.
	//

	// Start of user code Publisher_Kind_deploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: deploy -
	 * title:
	 */
	@Override
	public void deploy() {
		LOGGER.debug("Action deploy() called on " + this);
		compMan.deploy();
		// TODO: Implement how to deploy this publisher.
	}

	// End of user code
	// Start of user code Publisher_Kind_Stop_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: stop -
	 * title: Stop the application.
	 */
	@Override
	public void stop() {
		LOGGER.debug("Action stop() called on " + this);
		compMan.stop();
		// TODO: Implement how to stop this publisher.
	}

	// End of user code
	// Start of user code Publisher_Kind_Start_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: start -
	 * title: Start the application.
	 */
	@Override
	public void start() {
		setRuntimeInformation();
		System.out.println(this.attributes);
		LOGGER.debug("Action start() called on " + this);
		compMan.start();
		removeRuntimeInformation();
		System.out.println(this.attributes);
	}

	// End of user code
	// Start of user code Publisher_Kind_configure_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: configure
	 * - title:
	 */
	@Override
	public void configure() {
		LOGGER.debug("Action configure() called on " + this);
		compMan.configure();
		// TODO: Implement how to configure this publisher.
	}

	// End of user code
	// Start of user code Publisher_Kind_undeploy_action
	/**
	 * Implement OCCI action: - scheme:
	 * http://schemas.modmacao.org/occi/platform/component/action# - term: undeploy
	 * - title:
	 */
	@Override
	public void undeploy() {
		LOGGER.debug("Action undeploy() called on " + this);
		compMan.undeploy();
		// TODO: Implement how to undeploy this publisher.
	}
	// End of user code

	private MixinBase getMartMixin() {
		for (MixinBase mixinBase : this.getParts()) {
			if (mixinBase instanceof Occiresultprovider) {
				LOGGER.info("MartPublisher Mixin found for Publisher:" + this.title);
				return mixinBase;
			}
		}
		return null;
	}

	private List<Monitorableproperty> getMonitoringProperties(Sensor sens) {
		LinkedList<Monitorableproperty> monProps = new LinkedList<Monitorableproperty>();
		for (Link link : sens.getLinks()) {
			if (link instanceof Monitorableproperty) {
				monProps.add((Monitorableproperty) link);
			}
		}
		return monProps;
	}

	private void setRuntimeInformation() {
		sensor = getSensor();
		AttributeState sensorAttr = factory.createAttributeState();
		sensorAttr.setName("sensor");
		sensorAttr.setValue(sensor.getLocation());
		System.out.println("Sensor: " + sensor);
		this.attributes.add(sensorAttr);
		workaround.add(sensorAttr);

		monProp = getMonProp(sensor);
		AttributeState monPropAttr = factory.createAttributeState();
		monPropAttr.setName("monitorable.property");
		monPropAttr.setValue(monProp.getLocation());
		System.out.println("MonProp: " + monProp);
		this.attributes.add(monPropAttr);
		workaround.add(monPropAttr);

		AttributeState monPropNameAttr = factory.createAttributeState();
		monPropNameAttr.setName("monitorable.property.property");
		monPropNameAttr.setValue(monProp.getMonitoringProperty());
		this.attributes.add(monPropNameAttr);
		workaround.add(monPropNameAttr);

		monObject = monProp.getTarget();
		AttributeState monObjectAttr = factory.createAttributeState();
		monObjectAttr.setName("monitorable.property.target");
		monObjectAttr.setValue(monObject.getLocation());
		this.attributes.add(monObjectAttr);
		workaround.add(monObjectAttr);
	}

	private Sensor getSensor() {
		for (Link link : this.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return ((Sensor) link.getSource());
			}
		}
		throw new NoSuchElementException("No containing sensor found!");
	}

	private Monitorableproperty getMonProp(Sensor sensor) {
		for (Link link : sensor.getLinks()) {
			if (link instanceof Monitorableproperty) {
				return ((Monitorableproperty) link);
			}
		}
		throw new NoSuchElementException("No monitorableproperty found in sensor!");
	}

	private void removeRuntimeInformation() {
		for (AttributeState work : workaround) {
			this.attributes.remove(work);
		}

	}

	private String getContainingSensorLocation() {
		for (Link link : this.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return link.getSource().getLocation();
			}
		}
		return "No containing sensor found";
	}

	private Sensor getContainingSensor() {
		for (Link link : this.getRlinks()) {
			if (link.getSource() instanceof Sensor) {
				return (Sensor) link.getSource();
			}
		}
		LOGGER.info("No containing Sensor found");
		throw new NoSuchElementException();
	}
}
