# Monitoring Connector
Each element in the monitoring extension has a single file in the connector implementing how to react on different REST requests addressing the corresponding OCCI element.
As the elements of the monitoring extension mainly inherit from elements of the enhanced platform extension provided by [MoDMaCAO](https://github.com/occiware/MoDMaCAO), the implementation of the lifecycle actions is quite similar. To handle the management of each individual component of a sensor, configuration management scripts have to be attached to them. Thus, in comparison to the [dummy connector](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mocci/tree/master/de.ugoe.cs.rwm.mocci.connector.dummy), this connector allows for an actual deployment and management of sensors in a cloud environment. The skeleton for the connector of the OCCI monitoring extension is generated using [OCCI-Studio](https://github.com/occiware/OCCI-Studio).


## Attaching Configuration Management Scripts
To attach a configuration management script to a component, a user mixin serving as tag has to be created.
Moreover, the component managed over the defined script has to have this mixin attached.
Finally, the script itself is located on the MartServer, typically in a folder called roles, and has the same name
as the mixin tag.
The following figure gives an example of such an attachment. 
Here, the blue element is the User Mixin which is attached to the DataGatherer which is managed over lifecycle
actions as described in the configuration management script called glances, located in the roles folder of the MartServer.


![Attachment](./example.jpg "Attachment")

*Note:* Currently, MoDMaCAO only provides a connector for [ansible](https://docs.ansible.com/).
