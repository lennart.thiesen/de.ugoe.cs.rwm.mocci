package de.ugoe.cs.rwm.mocci;

import static org.junit.Assert.assertTrue;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.OCCIFactory;
import org.eclipse.cmf.occi.core.impl.OCCIFactoryImpl;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.InfrastructureFactory;
import org.eclipse.cmf.occi.infrastructure.impl.InfrastructureFactoryImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.occi.platform.PlatformFactory;
import org.modmacao.occi.platform.impl.PlatformFactoryImpl;

import monitoring.Datagatherer;
import monitoring.Dataprocessor;
import monitoring.Monitorableproperty;
import monitoring.MonitoringFactory;
import monitoring.Resultprovider;
import monitoring.Sensor;
import monitoring.impl.MonitoringFactoryImpl;

public class CreateMonitoringElementsTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void createMonitoringElements() {
		// Factories to create OCCI elements
		OCCIFactory cFact = new OCCIFactoryImpl();
		InfrastructureFactory iFact = new InfrastructureFactoryImpl();
		MonitoringFactory mFact = new MonitoringFactoryImpl();
		PlatformFactory pFact = new PlatformFactoryImpl();

		// Top level element
		Configuration config = cFact.createConfiguration();
		Compute comp = iFact.createCompute();
		// Resources
		Sensor sensor = mFact.createSensor();
		config.getResources().add(sensor);
		config.getResources().add(comp);

		Monitorableproperty mp = mFact.createMonitorableproperty();
		mp.setSource(sensor);
		mp.setTarget(comp);

		Datagatherer dg = mFact.createDatagatherer();
		Componentlink cl = pFact.createComponentlink();
		cl.setSource(sensor);
		cl.setTarget(dg);

		Dataprocessor dp = mFact.createDataprocessor();
		Componentlink cl1 = pFact.createComponentlink();
		cl1.setSource(sensor);
		cl1.setTarget(dp);

		Resultprovider rp = mFact.createResultprovider();
		Componentlink cl2 = pFact.createComponentlink();
		cl2.setSource(sensor);
		cl2.setTarget(rp);

		System.out.println("Checking configuration: " + config.getResources());

		assertTrue(checkSensor(config));

	}

	public boolean checkSensor(Configuration conf) {
		for (org.eclipse.cmf.occi.core.Resource res : conf.getResources()) {
			if (res instanceof Sensor) {
				Sensor sens = (Sensor) res;
				if (containsDevices(sens)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean containsDevices(Sensor sens) {
		int count = 0;
		for (Link link : sens.getLinks()) {
			if (link instanceof Componentlink) {
				count++;
			}
		}
		if (count == 3) {
			return true;
		}
		return false;
	}
}
