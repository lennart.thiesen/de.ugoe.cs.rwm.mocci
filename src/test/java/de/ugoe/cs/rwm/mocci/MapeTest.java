package de.ugoe.cs.rwm.mocci;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;

public class MapeTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.loggerSetup();
		TestUtility.extensionRegistrySetup();
	}

	@Before
	public void resetEverything() {
		CachedResourceSet.getCache().clear();
		Path occiPath = Paths.get(ModelUtility.getPathToResource("occi/Empty.occic"));
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn);
		deployer.deploy(occiPath);

		InitialDeployment.main(new String[0]);
	}

	@Test
	public void Monitor() {
		Monitor monitor = MAPE.monitor();
		assertTrue(monitor.getAllCPUs() == 1);
	}

	@Test
	public void Analyze() {
		Monitor monitor = MAPE.monitor();
		String analysis = MAPE.analyze(monitor);
		if (monitor.getCritCPUs() == 1) {
			assertTrue(analysis.equals("upScale"));
		}
		if (monitor.getNoneCPUs() == 1) {
			assertTrue(analysis.equals("downScale"));
		}
	}

	@Test
	public void Plan() {
		Monitor monitor = MAPE.monitor();
		String analysis = MAPE.analyze(monitor);
		Resource res = MAPE.plan(analysis);
		if (analysis.equals("upScale")) {
			Configuration pre = (Configuration) MAPE.runtimeModel.getContents().get(0);
			Configuration post = (Configuration) res.getContents().get(0);
			assertTrue(pre.getResources().size() < post.getResources().size());

		}
	}

	@Test
	public void Execute() {
		Monitor monitor = MAPE.monitor();
		String analysis = MAPE.analyze(monitor);
		MAPE.runtimeModel = MAPE.plan(analysis);
		MAPE.execute(MAPE.runtimeModel);
		assertTrue(TestUtility.equalsRuntime(MAPE.runtimeModel, MAPE.conn));
	}

	@Test
	public void Iterations() {
		int iterations = 20;
		int count = 0;
		int interval = 1000;
		while (count < iterations) {
			try {
				System.out.println("\n--------------------Waiting for new MAPE-K Cycle: Sleeping " + interval
						+ "--------------------");
				Thread.sleep(interval);

				Monitor monitor = MAPE.monitor();
				String analysis = MAPE.analyze(monitor);
				MAPE.runtimeModel = MAPE.plan(analysis);
				MAPE.execute(MAPE.runtimeModel);
				count++;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

}
