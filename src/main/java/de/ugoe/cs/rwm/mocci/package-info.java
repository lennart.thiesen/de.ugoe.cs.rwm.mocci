/**
 * Main package that includes util classes as well as the monitoring interface.
 *
 * @author erbel
 *
 */
package de.ugoe.cs.rwm.mocci;
