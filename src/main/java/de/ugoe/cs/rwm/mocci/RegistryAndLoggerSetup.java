/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.crtp.CrtpPackage;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.modmacao.ansibleconfiguration.AnsibleconfigurationPackage;
import org.modmacao.occi.platform.PlatformPackage;
import org.modmacao.placement.PlacementPackage;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.appdeployer.MartAppDeployerSlave;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.tocci.Transformator;
import modmacao.ModmacaoPackage;
import monitoring.MonitoringPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;

public class RegistryAndLoggerSetup {

	static String manNWid = "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339590";
	static String manNWRuntimeId = "75a4639e-9ce7-4058-b859-8a711b0e2e7b";
	static String sshKey = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6H7Ydi45BTHid4qNppGAi5mzjbnZgt7bi6xLGmZG9CiLmhMsxOuk3Z05Nn+pmoN98qS0eY8S240PPk5VOlYqBY0vdRAwrZSHHaLdMp6I7ARNrI2KraYduweqz7ZQxPXQfwIeYx2HKQxEF2r+4//Fo4WfgdBkLuulvl/Gw3TUzJNQHvgpaiNo9+PI5CZydHnZbjUkRikS12pT+CbNKj+0QKeQztbCd41aKxDv5H0DjltVRcpPppv4dmiU/zoCAIngWLO1PPgfYWyze8Z9IoyBT7Qdg30U91TYZBuxzXR5lq7Fh64y/IZ/SjdOdSIvIuDjtmJDULRdLJzrvubrKY+YH Generated-by-Nova";
	static String userData = "I2Nsb3VkLWNvbmZpZwoKIyBVcGdyYWRlIHRoZSBpbnN0YW5jZSBvbiBmaXJzdCBib290CiMgKGllIHJ1biBhcHQtZ2V0IHVwZ3JhZGUpCiMKIyBEZWZhdWx0OiBmYWxzZQojIEFsaWFzZXM6IGFwdF91cGdyYWRlCnBhY2thZ2VfdXBncmFkZTogdHJ1ZQoKcGFja2FnZXM6CiAtIHB5dGhvbgoKd3JpdGVfZmlsZXM6CiAgLSBwYXRoOiAvZXRjL25ldHdvcmsvaW50ZXJmYWNlcy5kLzUwLWNsb3VkLWluaXQuY2ZnCiAgICBjb250ZW50OiB8CiAgICAgIGF1dG8gbG8KICAgICAgaWZhY2UgbG8gaW5ldCBsb29wYmFjawogICAgICAKICAgICAgYXV0byBlbnMwCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMAogICAgICBpZmFjZSBlbnMwIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMQogICAgICBpZmFjZSBlbnMxIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMyCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMgogICAgICBpZmFjZSBlbnMyIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMzCiAgICAgIGFsbG93LWhvdHBsdWcgZW5zMwogICAgICBpZmFjZSBlbnMzIGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM0CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNAogICAgICBpZmFjZSBlbnM0IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM1CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNQogICAgICBpZmFjZSBlbnM1IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM2CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNgogICAgICBpZmFjZSBlbnM2IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM3CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zNwogICAgICBpZmFjZSBlbnM3IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM4CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOAogICAgICBpZmFjZSBlbnM4IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnM5CiAgICAgIGFsbG93LWhvdHBsdWcgZW5zOQogICAgICBpZmFjZSBlbnM5IGluZXQgZGhjcAogICAgICAKICAgICAgYXV0byBlbnMxMAogICAgICBhbGxvdy1ob3RwbHVnIGVuczEwCiAgICAgIGlmYWNlIGVuczEwIGluZXQgZGhjcAoKIyMj";

	public static void setup() {
		loggerSetup();
		registrySetup();
	}

	private static void loggerSetup() {
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(ModelRetriever.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Deprovisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Executor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(MartAppDeployerSlave.class.getName()).setLevel(Level.INFO);
	}

	private static void registrySetup() {

		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OpenstackruntimePackage.eINSTANCE.eClass();

		PlacementPackage.eINSTANCE.eClass();
		OssweruntimePackage.eINSTANCE.eClass();
		AnsibleconfigurationPackage.eINSTANCE.eClass();
		MonitoringPackage.eINSTANCE.eClass();
		PlatformPackage.eINSTANCE.eClass();
		CrtpPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/platform#",
				PlatformPackage.class.getClassLoader().getResource("model/platform.occie").toString());

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/modmacao#",
				ModmacaoPackage.class.getClassLoader().getResource("model/modmacao.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/runtime#",
				OpenstackruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OssweruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/placement#",
				PlacementPackage.class.getClassLoader().getResource("model/placement.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OCCIPackage.class.getClassLoader().getResource("model/ossweruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/occi/ansible#",
				OCCIPackage.class.getClassLoader().getResource("model/ansibleconfiguration.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/monitoring#",
				MonitoringPackage.class.getClassLoader().getResource("model/monitoring.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure/compute/template/1.1#",
				OCCIPackage.class.getClassLoader().getResource("model/crtp.occie").toString());

		// Registry.INSTANCE.getExtensionToFactoryMap().put("*", new
		// OCCIResourceFactoryImpl());

	}
}
