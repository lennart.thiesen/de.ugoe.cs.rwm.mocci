/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import java.nio.file.Path;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.ComputeStatus;
import org.eclipse.cmf.occi.infrastructure.Ipnetworkinterface;
import org.eclipse.cmf.occi.infrastructure.Networkinterface;
import org.eclipse.emf.ecore.resource.Resource;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;
import org.modmacao.placement.Placementlink;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import monitoring.Datagatherer;
import monitoring.Dataprocessor;
import monitoring.Monitorableproperty;
import monitoring.Occiresultprovider;
import monitoring.Resultprovider;
import monitoring.Sensor;
import ossweruntime.Ubuntu_xenialxerus;

public class UpScaler extends AbsScaler {

	public UpScaler(Connector conn, Path runtimePath) {
		this.conn = conn;
		this.runtimePath = runtimePath;
	}

	@SuppressWarnings("unused")
	public Resource upScaleNodes() {
		runtimeModel = ModelUtility.loadOCCIintoEMFResource(conn.loadRuntimeModel(runtimePath));
		Configuration config = ((Configuration) runtimeModel.getContents().get(0));
		Compute monVM = (Compute) getResourceById(config.getResources(),
				"urn:uuid:37829092-c690-494a-98fa-335b2fd660ea");

		Compute comp = addCompute(config);
		Component worker = addWorkerComponent(config, comp);
		Sensor sens = addSensor(config, comp);
		Datagatherer dg = addDataGatherer(config, comp, sens);
		Dataprocessor dp = addDataProcessor(config, comp, sens, dg);
		Resultprovider rp = addResultProvider(config, comp, sens, dp);

		return runtimeModel;
	}

	private Compute addCompute(Configuration config) {
		System.out.println("         Adding Compute Node to Model");
		Compute comp = iFactory.createCompute();
		comp.setOcciComputeState(ComputeStatus.ACTIVE);

		AttributeState state = factory.createAttributeState();
		state.setName("occi.compute.state");
		state.setValue("active");
		comp.getAttributes().add(state);
		config.getResources().add(comp);

		Ubuntu_xenialxerus mixOs = osFactory.createUbuntu_xenialxerus();
		comp.getParts().add(mixOs);

		Networkinterface nwi = iFactory.createNetworkinterface();
		nwi.setTarget(getResourceById(config.getResources(), "urn:uuid:29d78078-fb4c-47aa-a9af-b8aaf3339591"));
		nwi.setSource(comp);
		Ipnetworkinterface mixB = iFactory.createIpnetworkinterface();
		String ip = interfaces.get(0);
		mixB.setOcciNetworkinterfaceAddress(ip);
		nwi.getParts().add(mixB);
		AttributeState ipAttr = factory.createAttributeState();
		ipAttr.setName("occi.networkinterface.address");
		ipAttr.setValue(ip);
		nwi.getAttributes().add(ipAttr);

		String ipadjusted = ip.replace(".", "-");
		comp.setTitle("hadoop-worker-additional-" + ipadjusted);

		AttributeState hostname = factory.createAttributeState();
		hostname.setName("occi.compute.hostname");
		hostname.setValue("hadoop-worker-additional-" + ipadjusted);
		comp.getAttributes().add(hostname);

		System.out.println("             Ip in Hadoop Network: " + interfaces.get(0));
		interfaces.remove(0);

		Networkinterface nwimon = iFactory.createNetworkinterface();
		nwimon.setTarget(getResourceById(config.getResources(), "urn:uuid:7a9fca2c-24fb-473c-aa9c-8dc9e68a432a"));
		nwimon.setSource(comp);
		return comp;
	}

	private Component addWorkerComponent(Configuration config, Compute comp) {
		System.out.println("         Adding Worker Component to Model");
		Component worker = pFactory.createComponent();
		worker.setTitle("worker-component");
		// worker.setOcciComponentState(Status.ACTIVE);
		config.getResources().add(worker);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(worker.getId());
		worker.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("hworker", config));
		worker.getParts().add(mBase);

		Placementlink pLink = placeFactory.createPlacementlink();
		pLink.setSource(worker);
		pLink.setTarget(comp);

		Componentlink compLink = pFactory.createComponentlink();
		compLink.setSource(getResourceById(config.getResources(), "urn:uuid:a4888ba9-a0ea-48f2-a29e-901c876ab42d"));
		compLink.setTarget(worker);
		MAPE.newComp = worker;
		return worker;
	}

	private Sensor addSensor(Configuration config, Compute comp) {
		System.out.println("         Adding Sensor to Model");
		Sensor sens = mFactory.createSensor();
		sens.setTitle("CPUSensor");
		config.getResources().add(sens);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(sens.getId());
		sens.getAttributes().add(attr);

		Monitorableproperty mp = mFactory.createMonitorableproperty();
		mp.setTitle("monProp");
		mp.setMonitoringProperty("CPU");
		AttributeState attrProp = factory.createAttributeState();
		attrProp.setName("monitoring.property");
		attrProp.setValue("CPU");
		mp.getAttributes().add(attrProp);
		mp.setSource(sens);
		mp.setTarget(comp);

		return sens;
	}

	private Datagatherer addDataGatherer(Configuration config, Compute comp, Sensor sens) {
		System.out.println("             Adding Datagatherer to Model");
		Datagatherer dg = mFactory.createDatagatherer();
		dg.setTitle("CPUGatherer");
		config.getResources().add(dg);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(dg.getId());
		dg.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpugatherer", config));
		dg.getParts().add(mBase);

		Componentlink c1 = pFactory.createComponentlink();
		c1.setSource(sens);
		c1.setTarget(dg);

		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(dg);
		pl.setTarget(comp);

		return dg;
	}

	private Dataprocessor addDataProcessor(Configuration config, Compute comp, Sensor sens, Datagatherer dg) {
		System.out.println("             Adding Dataprocessor to Model");
		Dataprocessor dp = mFactory.createDataprocessor();
		dp.setTitle("CPUProcessor");
		config.getResources().add(dp);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(dp.getId());
		dp.getAttributes().add(attr);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpuprocessorlocal", config));
		dp.getParts().add(mBase);

		Componentlink c2 = pFactory.createComponentlink();
		c2.setSource(sens);
		c2.setTarget(dp);
		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(dp);
		pl.setTarget(comp);
		Componentlink cl = pFactory.createComponentlink();
		cl.setSource(dp);
		cl.setTarget(dg);
		return dp;
	}

	private Resultprovider addResultProvider(Configuration config, Compute comp, Sensor sens, Dataprocessor dp) {
		System.out.println("             Adding Resultprovider to Model");
		Resultprovider rp = mFactory.createResultprovider();
		rp.setTitle("CPUProvider");
		config.getResources().add(rp);

		Occiresultprovider orp = mFactory.createOcciresultprovider();
		orp.setResultProviderEndpoint("192.168.35.45:8080");
		AttributeState attrOP = factory.createAttributeState();
		attrOP.setName("result.provider.endpoint");
		attrOP.setValue("192.168.35.45:8080");
		rp.getAttributes().add(attrOP);

		rp.getParts().add(orp);

		MixinBase mBase = factory.createMixinBase();
		mBase.setMixin(getMixin("cpupublisher", config));
		rp.getParts().add(mBase);

		AttributeState attr = factory.createAttributeState();
		attr.setName("occi.core.id");
		attr.setValue(rp.getId());
		rp.getAttributes().add(attr);

		Componentlink c3 = pFactory.createComponentlink();
		c3.setSource(sens);
		c3.setTarget(rp);

		Placementlink pl = placeFactory.createPlacementlink();
		pl.setSource(rp);
		pl.setTarget(comp);

		Componentlink cl = pFactory.createComponentlink();
		cl.setSource(rp);
		cl.setTarget(dp);

		return rp;
	}

	private Mixin getMixin(String string, Configuration config) {
		for (Mixin mix : config.getMixins()) {
			if (mix.getTerm().equals(string)) {
				return mix;
			}
		}
		return null;
	}
}
