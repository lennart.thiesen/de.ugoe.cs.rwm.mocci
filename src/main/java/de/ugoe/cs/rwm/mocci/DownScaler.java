/**
 * Copyright (c) 2018-2019 University of Goettingen
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 */

package de.ugoe.cs.rwm.mocci;

import java.nio.file.Path;
import java.util.List;

import org.eclipse.cmf.occi.core.AttributeState;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.Networkinterface;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.modmacao.occi.platform.Component;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import monitoring.Monitorableproperty;

public class DownScaler extends AbsScaler {

	private List<Link> linksToDelete;
	private List<org.eclipse.cmf.occi.core.Resource> resourcesToDelete;

	public DownScaler(Connector conn, Path runtimePath) {
		this.conn = conn;
		this.runtimePath = runtimePath;
		this.linksToDelete = new BasicEList<Link>();
		this.resourcesToDelete = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
	}

	public Resource downScaleNodes() {
		CachedResourceSet.getCache().clear();
		runtimeModel = ModelUtility.loadOCCIintoEMFResource(conn.loadRuntimeModel(runtimePath));
		Configuration config = ((Configuration) runtimeModel.getContents().get(0));
		boolean downScale = false;

		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if (res instanceof Compute) {
				Compute comp = (Compute) res;
				if (comp.getTitle().contains("hadoop-worker-additional")) {
					Monitorableproperty monProp = getAttachedCPUMonProp(comp);
					if (monProp != null && monProp.getMonitoringResult() != null) {
						if (monProp.getMonitoringResult().equals("None")) {
							System.out.println("      VM with None CPU utilization found: " + comp.getId());
							addConnectedLinksAndComponents(comp);
							resourcesToDelete.add(comp);
							System.out.println(
									"      Delete Entities Around: " + comp.getTitle() + " (" + comp.getId() + ")");
							downScale = true;
							break;
						}
					}
				}
			}
		}

		if (downScale == false) {
			System.out.println("      Every Compute busy/Only one worker! Skipping downScale!");
		}

		for (Link link : linksToDelete) {
			EcoreUtil.delete(link);
		}

		config.getResources().removeAll(resourcesToDelete);

		for (org.eclipse.cmf.occi.core.Resource res : resourcesToDelete) {
			EcoreUtil.delete(res);
		}
		Resource rM = runtimeModel;
		MAPE.newComp = null;
		CachedResourceSet.getCache().clear();
		return rM;

	}

	private Monitorableproperty getAttachedCPUMonProp(Compute comp) {
		for (Link link : comp.getRlinks()) {
			if (link instanceof Monitorableproperty) {
				Monitorableproperty monProp = (Monitorableproperty) link;
				if (monProp.getMonitoringProperty().equals("CPU")) {
					return monProp;
				}
			}
		}
		return null;
	}

	private void addConnectedLinksAndComponents(Compute comp) {
		linksToDelete.addAll(comp.getLinks());
		linksToDelete.addAll(comp.getRlinks());

		for (Link link : comp.getRlinks()) {
			if (link.getSource() instanceof Component) {
				resourcesToDelete.add(link.getSource());
				linksToDelete.addAll(link.getSource().getLinks());
				linksToDelete.addAll(link.getSource().getRlinks());
			}
			if (link instanceof Monitorableproperty) {
				resourcesToDelete.add(link.getSource());
			}
		}

		for (Link link : comp.getLinks()) {
			if (link instanceof Networkinterface) {
				Networkinterface nwi = (Networkinterface) link;
				for (AttributeState attr : nwi.getAttributes()) {
					if (attr.getName().equals("occi.networkinterface.address")) {
						if (attr.getValue().startsWith("10.254.1")) {
							interfaces.add(attr.getValue());
						}
					}
				}
				/*
				 * for(MixinBase mixB: nwi.getParts()) { if(mixB instanceof Ipnetworkinterface)
				 * { Ipnetworkinterface ipnwi = (Ipnetworkinterface) mixB;
				 * if(ipnwi.getOcciNetworkinterfaceAddress().startsWith("100.254.1")) {
				 * interfaces.add(ipnwi.getOcciNetworkinterfaceAddress()); } } }
				 */
			}
		}
	}

}
